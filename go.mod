module gitlab.com/timeterm/cup-svc

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/gogo/protobuf v1.3.0
	github.com/joho/godotenv v1.3.0
	github.com/onsi/ginkgo v1.10.1 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/rs/zerolog v1.15.0
	gitlab.com/timeterm/env v0.0.0-20191001061817-01717c815dce
	gitlab.com/timeterm/proto/go v0.0.0-20191010055501-a2552a4fe2e0
	golang.org/x/net v0.0.0-20191007182048-72f939374954
	golang.org/x/sys v0.0.0-20191008105621-543471e840be // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20191007204434-a023cd5227bd // indirect
	google.golang.org/grpc v1.24.0
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
