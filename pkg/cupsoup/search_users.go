package cupsoup

import (
	"context"
	"net/url"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/rs/zerolog/log"
)

func (c *Client) SearchUsers(ctx context.Context, firstLetters string) (map[string]string, error) {
	doc, err := c.doDocumentRequest(ctx, request{
		method: "POST",
		path:   PageDefault,
		extraFields: url.Values{
			"_nameTextBox":         {strings.ToLower(firstLetters)},
			"numberOfLettersField": {strconv.Itoa(len(firstLetters))},
			"_zoekButton":          {"Zoek"},
		},
	})
	if err != nil {
		return nil, err
	}

	ret := make(map[string]string)
	doc.Find("#_nameDropDownList option").Each(func(_ int, s *goquery.Selection) {
		index, indexExists := s.Attr("value")
		name := s.Text()

		if !indexExists {
			log.Warn().Msg("Could not find field index")
			return
		}
		if _, err := strconv.Atoi(index); err != nil {
			log.Warn().Str("index", index).Msg("Expected index to be a valid integer")
		}

		ret[index] = name
	})

	return ret, nil
}
