package model

import "time"

type Appointment struct {
	// Whether the CUP user cannot modify the appointment.
	Fixed bool
	Slot  int

	StartDate time.Time
	EndDate   time.Time

	Selected *Option
	Choices  []Choice
}

type Choice struct {
	Option     Option
	Full       bool
	InternalID int
}

type Option struct {
	Info string
	Text string

	AvailablePlaces int
}

type HistoryOption struct {
	Option

	Date time.Time
	Slot int
}

type Timetable map[int][]HistoryOption

type PreColumnImageInfo struct {
	IsError bool
	Msg     string
}
