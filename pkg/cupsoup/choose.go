package cupsoup

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/parse"
)

const preColumnImageSelector = ".prekolom > img"

type ChooseError struct {
	Reason string
}

func (c ChooseError) Error() string {
	return c.Reason
}

func (c *Client) Choose(ctx context.Context, internalID int) error {
	if !c.session.Valid() {
		return ErrInvalidSession
	}

	doc, err := c.doDocumentRequest(ctx, request{
		method: http.MethodPost,
		path:   PageRoosterForm,
		extraFields: url.Values{
			string(ASPFieldEventArgument): {strconv.Itoa(internalID)},
			"__SCROLLPOSITIONX":           {"0"},
			"__SCROLLPOSITIONY":           {"0"},
			"lastPosition":                {""},
			"txtLLtekst":                  {""},
			"hiddenlesnr":                 {""},
			"rememberClickY":              {"185"},
		},
		withoutASPFields: []ASPField{
			ASPFieldEventArgument,
		},
	})
	if err != nil {
		return fmt.Errorf("could not load %v: %w", PageRoosterForm, err)
	}

	imgs := doc.Find(preColumnImageSelector)
	imgs.EachWithBreak(func(_ int, img *goquery.Selection) bool {
		var info model.PreColumnImageInfo
		info, err = parse.PreColumnImage(img)
		if err != nil {
			return false
		}

		if info.IsError {
			err = ChooseError{
				Reason: info.Msg,
			}
			return false
		}

		return true
	})

	return err
}
