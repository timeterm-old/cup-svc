package parse

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
)

func AppointmentTable(s *goquery.Selection) (model.Appointment, error) {
	var err error
	var a model.Appointment

	s.ChildrenFiltered("td").EachWithBreak(func(col int, s *goquery.Selection) bool {
		err = appointmentColumn(col, s, &a)
		return err == nil
	})

	if err != nil {
		return model.Appointment{}, err
	}
	return a, nil
}

func appointmentColumn(col int, s *goquery.Selection, a *model.Appointment) error {
	// Case 2 is skipped, for this school the column is not shown (class: `roosterles`);
	// Case 4 is also skipped, this column is only use after the user has made a new selection (class: `prekolom`).

	switch col {
	case 0:
		return appointmentDate(s, a)
	case 1:
		return appointmentSlot(s, a)
	case 3:
		return appointmentSelection(s, a)
	case 5:
		return appointmentChoices(s, a)
	}

	return nil
}

func appointmentDate(s *goquery.Selection, a *model.Appointment) error {
	text := strings.TrimSpace(s.Text())

	startDate, err := timeParse(dateLayout, text)
	if err != nil {
		return err
	}
	a.StartDate = startDate

	// Set the end date to this time as well, the correct time is actually
	// set by appointmentSlot.
	a.EndDate = startDate

	return nil
}

func appointmentSelection(s *goquery.Selection, a *model.Appointment) error {
	a.Fixed = s.HasClass("fixedLesson")

	opt, err := AppointmentOption(s.Children())
	if err != nil {
		return fmt.Errorf("could not parse current selection: %w", err)
	}
	a.Selected = &opt

	return nil
}

// appointmentSlot parses the second column (the slot) in the selection table.
// To the user, this is presented as a two-letter day code with the slot number after it (e.g. ma2), meaning
// Monday, the second period. If the user hovers over this text, a tooltip is shown with the start and end time
// of the period (e.g. 9:20-10:10), meaning the period starts at 9:20 AM and ends at 10:10 AM.
// The day in the slot code is not parsed, as the date is presented in the leading column.
func appointmentSlot(s *goquery.Selection, a *model.Appointment) error {
	text := strings.TrimSpace(s.Text())
	if len(text) < 3 {
		return errors.New("expected day with period (e.g. ma2)")
	}

	slot, err := strconv.Atoi(text[2:])
	if err != nil {
		return fmt.Errorf("could not parse slot period: %w", err)
	}

	tooltipJS, exists := s.ChildrenFiltered("p").First().Attr("onmouseover")
	if exists {
		rawHelpText, err := strconv.Unquote(tillLast(fromFirst(tooltipJS, `"`), `"`))
		if err != nil {
			return fmt.Errorf("could not unquote tooltip JavaScript string literal: %w", err)
		}

		// Split into parts, so for example "9.20-10.10" gets split into "9.20" and "10.10".
		timeParts := strings.Split(rawHelpText, "-")
		if len(timeParts) == 2 {
			start, err := timeParse(timeLayout, timeParts[0])
			if err != nil {
				return fmt.Errorf("could not parse start time: %w", err)
			}

			end, err := timeParse(timeLayout, timeParts[1])
			if err != nil {
				return fmt.Errorf("could not parse end time: %w", err)
			}

			a.StartDate = withMinute(withHour(a.StartDate, start.Hour()), start.Minute())
			a.EndDate = withMinute(withHour(a.EndDate, end.Hour()), end.Minute())
		}
	}

	a.Slot = slot

	return nil
}

func appointmentChoices(s *goquery.Selection, a *model.Appointment) error {
	domChoices := s.Find(".choiceItem > .clsButton")
	newChoices := make([]model.Choice, len(a.Choices)+domChoices.Size())
	copy(newChoices, a.Choices)
	a.Choices = newChoices

	domChoices.Each(func(i int, choice *goquery.Selection) {
		full := !hasAttr(choice, "onmouseout") && !hasAttr(choice, "onmouseover")
		onClick, exists := choice.Attr("onclick")
		if !exists {
			return
		}

		onClick = strings.ReplaceAll(onClick, "keuzelesClicked(", "")
		params := strings.Split(onClick, ",")
		if len(params) == 0 {
			return
		}

		internalID, err := strconv.Atoi(params[0])
		if err != nil {
			return
		}

		option, err := AppointmentOption(choice)
		if err != nil {
			return
		}

		if a.Selected != nil {
			if a.Selected.Info == option.Info && a.Selected.Text == option.Text {
				a.Selected.AvailablePlaces = option.AvailablePlaces
			}
		}

		a.Choices[i] = model.Choice{
			Full:       full,
			Option:     option,
			InternalID: internalID,
		}
	})

	return nil
}

func hasAttr(s *goquery.Selection, a string) bool {
	_, has := s.Attr(a)
	return has
}

func withHour(t time.Time, hour int) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), hour, t.Minute(), t.Second(), t.Nanosecond(), t.Location())
}

func withMinute(t time.Time, minute int) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), minute, t.Second(), t.Nanosecond(), t.Location())
}
