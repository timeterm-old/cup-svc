package parse

import (
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/rs/zerolog/log"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
)

func PrintableTimetable(s *goquery.Selection) (model.Timetable, error) {
	var skipNextAppointment bool
	var currentWeek int
	var err error

	timetable := make(model.Timetable)

	rows := s.Find(".StandaardTabel > tbody > tr")

	if rows.Size() < 1 {
		return nil, ErrNoData
	}

	rows.Slice(1, goquery.ToEnd).Each(func(_ int, row *goquery.Selection) {
		var weekAppointments []model.HistoryOption
		columns := row.ChildrenFiltered("td")

		if columns.Size() == 1 {
			currentWeek, err = strconv.Atoi(
				strings.TrimSpace(
					strings.TrimPrefix(columns.First().Text(), "week : "),
				),
			)
			if err != nil {
				currentWeek = 1
			}

			skipNextAppointment = true
			return
		}

		if skipNextAppointment {
			skipNextAppointment = false
			return
		}

		weekAppointments = timetable[currentWeek]

		dateText := columns.First().Text()
		date, err := timeParse(dateLayout, strings.ReplaceAll(dateText, "\u00a0", ""))
		if err != nil {
			return
		}

		columns = columns.Slice(2, goquery.ToEnd)

		columns.Each(func(i int, column *goquery.Selection) {
			text := strings.TrimSpace(strings.ReplaceAll(column.Text(), "\u00a0", ""))
			if text != "" {
				opt, err := AppointmentOption(column.Children())
				if err != nil {
					log.Error().Err(err).Msg("Could not parse appointment option")
				}
				weekAppointments = append(weekAppointments, model.HistoryOption{
					Option: opt,
					Date:   date,
					Slot:   i + 1,
				})
			}
		})

		timetable[currentWeek] = weekAppointments
	})

	return timetable, nil
}
