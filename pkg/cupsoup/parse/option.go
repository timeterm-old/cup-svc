package parse

import (
	"bytes"
	"strconv"
	"strings"

	"github.com/rs/zerolog/log"

	"golang.org/x/net/html"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
)

func AppointmentOption(s *goquery.Selection) (model.Option, error) {
	opt := model.Option{}

	parts := strings.Split(textifyOption(s.Nodes), " ")
	for _, part := range parts {
		part = strings.TrimSpace(part)
		if part == "" {
			continue
		}

		if strings.HasPrefix(part, "[") && strings.HasSuffix(part, "]") && len(part) > 0 {
			var err error
			opt.AvailablePlaces, err = strconv.Atoi(strings.Trim(part, "[]"))
			if err != nil {
				log.Warn().
					Err(err).
					Interface("optionText", s.Text()).
					Msg("Could not parse available places count as integer")
			}

			continue
		}

		if opt.Text != "" {
			opt.Text += " "
		}
		opt.Text += strings.TrimSpace(part)
	}

	imgRawMessage, exists := s.Find("img").First().Attr("onmouseover")
	if exists {
		quoted := tillLast(fromFirst(imgRawMessage, "'"), "'")
		msg, err := unquote(quoted)
		if err != nil {
			return opt, err
		}

		// This may incur a significant slowdown :P
		buf := bytes.NewBufferString(msg)
		doc, err := goquery.NewDocumentFromReader(buf)
		if err != nil {
			return opt, err
		}

		body := doc.Find("html > body")
		opt.Info = strings.TrimSpace(textifyOption(body.Nodes))
	}

	return opt, nil
}

func textifyOption(nodes []*html.Node) string {
	var buf bytes.Buffer

	var i int
	for node := nodes[0].FirstChild; node != nil; node = node.NextSibling {
		if node.Type != html.TextNode {
			continue
		}

		if i > 0 {
			buf.WriteByte(' ')
		}
		buf.WriteString(node.Data)
		i++
	}

	return buf.String()
}
