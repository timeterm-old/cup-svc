package cupsoup

type cupsoupError string

func (e cupsoupError) Error() string {
	return string(e)
}

const (
	ErrInvalidSession cupsoupError = "invalid session"
)
