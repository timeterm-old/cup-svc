package cupsoup

const (
	defaultHost = "ccgobb.cupweb6.nl"
	defaultUA   = "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0"
)

type Opt func(*opts)

type opts struct {
	host string
	sess Session
}

func getOpts(options []Opt) *opts {
	o := opts{
		host: defaultHost,
	}

	for _, opt := range options {
		opt(&o)
	}

	return &o
}

func WithHost(h string) Opt {
	return func(o *opts) {
		o.host = h
	}
}

func WithSession(s Session) Opt {
	return func(o *opts) {
		o.sess = s
	}
}
