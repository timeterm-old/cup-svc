package cupsoup

import (
	"net/url"

	"github.com/PuerkitoBio/goquery"
)

type ASPField string

const (
	ASPFieldEventArgument      ASPField = "__EVENTARGUMENT"
	ASPFieldEventTarget        ASPField = "__EVENTTARGET"
	ASPFieldEventValidation    ASPField = "__EVENTVALIDATION"
	ASPFieldViewState          ASPField = "__VIEWSTATE"
	ASPFieldViewStateGenerator ASPField = "__VIEWSTATEGENERATOR"
)

type ASPFields url.Values

func (a ASPFields) Without(f ...ASPField) ASPFields {
	estimatedLen := len(a) - len(f)
	if estimatedLen < 0 {
		estimatedLen = 0
	}
	ret := make(ASPFields, estimatedLen)

FieldLoop:
	for field, value := range a {
		// Check if the field should be ignored.
		for _, ignore := range f {
			if string(ignore) == field {
				continue FieldLoop
			}
		}

		ret[field] = value
	}

	return ret
}

func AllASPFields() []ASPField {
	return []ASPField{
		ASPFieldEventTarget,
		ASPFieldEventArgument,
		ASPFieldViewState,
		ASPFieldViewStateGenerator,
		ASPFieldEventValidation,
	}
}

func ExtractASPFields(d *goquery.Document) ASPFields {
	fields := AllASPFields()

	values := make(ASPFields, len(fields))
	for _, f := range fields {
		value, exists := findFieldValue(d, string(f))
		if exists {
			values[string(f)] = []string{value}
		} else {
			values[string(f)] = nil
		}
	}

	return values
}

func findFieldValue(d *goquery.Document, name string) (string, bool) {
	return d.Find("#" + name).Attr("value")
}
