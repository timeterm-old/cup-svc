package cupsoup

import (
	"context"
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type CaptchaError struct {
	URL string
	Err error
}

type CUPError string

func (c CUPError) Error() string {
	return strconv.Quote(string(c))
}

func (c CaptchaError) Error() string {
	return fmt.Sprintf("captcha solving required (url: %v): %v", c.URL, c.Err)
}

func (c *Client) getCaptchaImageURL(d *goquery.Document) (string, bool) {
	var imageURL string
	d.Find("img").Each(func(_ int, s *goquery.Selection) {
		altAttr, altAttrExists := s.Attr("alt")
		srcAttr, srcAttrExists := s.Attr("src")

		if altAttrExists && altAttr == "Captcha" && srcAttrExists {
			imageURL = c.newURL(srcAttr)
		}
	})
	return imageURL, imageURL != ""
}

func (c *Client) SignIn(ctx context.Context, dropdownOption, pin, captchaText string) error {
	extraFields := url.Values{
		"_nameDropDownList": {dropdownOption},
		"_pincodeTextBox":   {pin},
		"_loginButton":      {"Login"},
	}
	if captchaText != "" {
		extraFields["txtCaptcha"] = []string{captchaText}
	}

	rsp, err := c.do(ctx, request{
		method:      "POST",
		path:        PageLoginWebForm,
		extraFields: extraFields,
	})
	if err != nil {
		return err
	}
	defer closeResponseBody(rsp.Body)

	sessionToken, err := extractSessionToken(rsp.Request.URL.Path)
	if err != nil {
		return err
	}
	c.session.Token = sessionToken

	doc, err := goquery.NewDocumentFromReader(rsp.Body)
	if err != nil {
		return err
	}

	c.session.ASPFields = ExtractASPFields(doc)

	label := doc.Find("#_errorLabel")
	if label != nil && strings.TrimSpace(label.Text()) != "Error Label" {
		imageURL, ok := c.getCaptchaImageURL(doc)
		if ok {
			return CaptchaError{
				URL: imageURL,
				Err: fmt.Errorf("could not log in: %w", CUPError(label.Text())),
			}
		}

		return fmt.Errorf("could not log in: %w", CUPError(label.Text()))
	}

	return nil
}
