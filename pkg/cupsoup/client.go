package cupsoup

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type Client struct {
	client  *http.Client
	host    string
	session Session
}

type request struct {
	method           string
	path             string
	extraFields      url.Values
	withoutASPFields []ASPField
}

func NewClient(o ...Opt) *Client {
	// Ignore this error because cookiejar never returns an error.
	jar, _ := cookiejar.New(new(cookiejar.Options))

	opts := getOpts(o)

	cl := &Client{
		host:    opts.host,
		session: opts.sess,
		client: &http.Client{
			Jar:     jar,
			Timeout: time.Second * 30,
		},
	}
	cl.client.Transport = roundTripper{
		callbacks: []func(*http.Request){
			func(_ *http.Request) {
				cl.session.LastRequest = time.Now()
			},
		},
		roundTripper: http.DefaultTransport,
	}
	return cl
}

func (c *Client) newURL(path string) string {
	var buf bytes.Buffer

	buf.WriteString("https://")
	buf.WriteString(c.host)
	buf.WriteByte('/')
	if c.session.Token != "" {
		buf.WriteString(c.session.Token)
		buf.WriteByte('/')
	}
	buf.WriteString(path)

	return buf.String()
}

func urlValuesMerge(a url.Values, b ...url.Values) url.Values {
	if a == nil {
		a = make(url.Values)
	}

	for _, set := range b {
		for key, values := range set {
			for _, value := range values {
				a.Add(key, value)
			}
		}
	}

	return a
}

func (c *Client) doDocumentRequest(ctx context.Context, r request) (*goquery.Document, error) {
	rsp, err := c.do(ctx, r)
	if err != nil {
		return nil, err
	}
	defer closeResponseBody(rsp.Body)

	doc, err := goquery.NewDocumentFromReader(rsp.Body)
	if err != nil {
		return nil, err
	}
	c.session.ASPFields = ExtractASPFields(doc)

	return doc, nil
}

func (c *Client) do(ctx context.Context, r request) (*http.Response, error) {
	urlValuesMerged := urlValuesMerge(
		r.extraFields,
		c.session.ExtraFields,
		url.Values(c.session.ASPFields.Without(r.withoutASPFields...)),
	).Encode()

	var body io.Reader
	if r.method != http.MethodGet {
		body = bytes.NewBufferString(urlValuesMerged)
	}

	req, err := http.NewRequest(r.method, c.newURL(r.path), body)
	if err != nil {
		return nil, err
	}
	if r.method == "" {
		req.Method = http.MethodGet
	}

	req.Header.Add("User-Agent", defaultUA)
	if req.Method == http.MethodGet {
		req.URL.RawQuery = urlValuesMerged
	} else {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	rsp, err := c.client.Do(req.WithContext(ctx))
	return rsp, err
}
