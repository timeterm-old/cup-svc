# cup-svc

## Projectstructuur

Dit project houdt zich zoveel mogelijk aan [golang-standards/project-layout](https://github.com/golang-standards/project-layout)
als mogelijk. Neem dat als referentiepunt als het volgende niet duidelijk maakt waar iets hoort.

```
gitlab.com/timeterm/cup-svc
  cmd/              - Uitvoerbare bestanden
    cup-svc/        - De cup-svc zelf
      app/          - Code voor de cup-svc
    cup-svc-client/ - voor nu een test client om de cup-svc makkelijk te testen
      app/          - Code voor de cup-svc-client
  internal/         - Code voor alleen de cup-svc
    database/       - Code om met de database te communiceren
      daos/         - Data Access Objects - manieren om verschillende entiteiten op gestructureerde wijzen te gebruiken
      model/        - Beschrijvingen van entiteiten
        convert/    - Conversies van vreemde objecten naar database-entiteiten
    service/        - Basiscode voor het service
    transport/      - Communicatie met de buitenwereld
      grpc/         - gRPC-communicatie (met de client)
        convert/    - Conversies van vreemde entiteiten naar Protocol Buffers (voor gRPC)
  pkg/              - Code die ook door andere programma's geïmporteerd kan worden (zonder problemen)
    cupsoup/        - CUP uitlezen
      parse/        - CUP-pagina's lezen (zoals printbaar rooster)
      model/        - Entiteiten die voortkomen uit CUP (gemaakte keuzes, lessen)
    names/          - Functies voor het werken met namen (op het moment niet in gebruik)
  go.mod            - Dependencies
  go.sum            - Dependency sum (voor check)  
```

## Inloggen

Gebruik hiervoor `CUP.SignIn`. Wanneer er een fout optreedt bij het inloggen en er een captcha zichtbaar is wordt er in de trailer in de metadata een sleutel `cup-captcha-url` meegegeven die gebruikt kan worden om de captcha-afbeelding mee te sturen.

## Lokaal draaien

Start voor het draaien van cup-svc Redis, de database:

```bash
$ docker-compose up
```

Daarna moeten de omgevingsvariabelen voor de service nog geconfigureerd worden. Dit kan gedaan worden door `.env.example`
te kopieren naar `.env`. Dit bestand wordt automatisch geladen door het programma, zorg dat daarin de variabelen correct
zijn geconfigureerd.

Start daarna de service op:

```bash
$ go run cmd/cup-svc/*.go
```

## Docker image bouwen

````bash
$ cd cmd/cup-svc
$ docker build .
````
