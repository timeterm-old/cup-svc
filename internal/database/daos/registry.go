package daos

import (
	"fmt"

	"gitlab.com/timeterm/cup-svc/internal/database"
)

// globalInit is used for the default initialization of a new DAO registry and
// sets a few default DAOs.
var globalInit = func() (Registry, error) {
	// Register DAOs here.
	return make(Registry).Register(
		new(UserDAO),
	)
}

// Registry keeps a map of DAOs mapped by the collection each owns.
type Registry map[string]DAO

// NewRegistry creates a new DAO Registry with a default set of DAOs included.
// If the initialization of the Registry fails, an error is returned.
func NewRegistry(db *database.Database) (Registry, error) {
	r, err := globalInit()
	if err != nil {
		return nil, fmt.Errorf("could not initialize new registry: %w", err)
	}

	// Initialize every DAO with the database so it can query data.
	for _, dao := range r {
		dao.Init(db)
	}

	return r, nil
}

// Register register one or more DAOs in Registry `r`. If any DAO with the same collection as an
// already existing DAO is attempted to be added, an error is returned.
func (r Registry) Register(daos ...DAO) (Registry, error) {
	for _, dao := range daos {
		collection := dao.Collection()
		if _, exists := r[collection]; exists {
			return nil, fmt.Errorf("a DAO with collection %v already exists", collection)
		}
		r[collection] = dao
	}

	return r, nil
}

// GetDAO retrieves a DAO with collection `collection` from the registry. If such a DAO does not exist,
// false is returned. If the ok is true it should be assumed that the returned DAO is valid and initialized.
// Always prefer to use the Get*DAO function instead of this function (like GetUserDAO), as it eliminates
// the type assertion for the specific DAO.
func (r Registry) GetDAO(collection string) (d DAO, ok bool) {
	dao, exists := r[collection]
	return dao, exists
}
