package daos

import "gitlab.com/timeterm/cup-svc/internal/database"

// DAO represents a data access object.
type DAO interface {
	Collection() string
	Init(db *database.Database)
}
