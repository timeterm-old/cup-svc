package convert

import (
	"github.com/gogo/protobuf/types"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// SessionProtoToCupsoup converts pb.Session to a cupsoup.Session.
func SessionProtoToCupsoup(s *pb.Session) (cupsoup.Session, error) {
	var sess cupsoup.Session

	lastReq, err := types.TimestampFromProto(s.LastRequest)
	if err != nil {
		return sess, err
	}

	sess = cupsoup.Session{
		ASPFields:   toMultiMap(s.AspFields),
		ExtraFields: toMultiMap(s.ExtraFields),
		Token:       s.Token,
		LastRequest: lastReq,
	}
	return sess, nil
}

// SessionCupsoupToProto converts cupsoup.Session to a pb.Session.
func SessionCupsoupToProto(s cupsoup.Session) (*pb.Session, error) {
	lastReq, err := types.TimestampProto(s.LastRequest)
	if err != nil {
		return nil, err
	}

	return &pb.Session{
		AspFields:   toSingularMap(s.ASPFields),
		ExtraFields: toSingularMap(s.ExtraFields),
		Token:       s.Token,
		LastRequest: lastReq,
	}, nil
}

// toMultimap takes a map of strings mapped to strings and creates a map
// of strings mapped to slices of single strings.
func toMultiMap(m map[string]string) map[string][]string {
	mm := make(map[string][]string, len(m))
	for k, v := range m {
		mm[k] = []string{v}
	}
	return mm
}

// toSingularMap takes a map of strings mapped to slices of strings and
// creates a map of strings mapped to strings (always taking the first non-empty
// element of the slice, if any).
func toSingularMap(m map[string][]string) map[string]string {
	sm := make(map[string]string, len(m))
	for k, v := range m {
		for _, val := range v {
			if val != "" {
				sm[k] = val
				break
			}
		}
	}
	return sm
}
