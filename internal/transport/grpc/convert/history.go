package convert

import (
	"github.com/gogo/protobuf/types"

	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// HistoryOptionCupsoupToProto converts model.HistoryOption to a pb.HistoryOption.
func HistoryOptionCupsoupToProto(h *model.HistoryOption) (*pb.HistoryOption, error) {
	option := OptionCupsoupToProto(h.Option)
	date, err := types.TimestampProto(h.Date)
	if err != nil {
		return nil, nil
	}

	return &pb.HistoryOption{
		Option: option,
		Date:   date,
		Slot:   int32(h.Slot),
	}, nil
}
