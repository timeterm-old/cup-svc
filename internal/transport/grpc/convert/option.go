package convert

import (
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup/model"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// OptionCupsoupToProto converts model.Option to a pb.Option.
func OptionCupsoupToProto(o model.Option) *pb.Option {
	return &pb.Option{
		Info:            o.Info,
		Text:            o.Text,
		AvailablePlaces: int32(o.AvailablePlaces),
	}
}
