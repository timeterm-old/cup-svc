package grpctx

import (
	"context"

	"gitlab.com/timeterm/cup-svc/internal/transport/grpc/convert"
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// PendingAppointments retrieves pending appointments from CUP.
// See the equivalent in cupsoup: cupsoup.Client.PendingAppointments.
func (c CUPServer) PendingAppointments(ctx context.Context,
	req *pb.PendingAppointmentsRequest,
) (*pb.PendingAppointmentsResponse, error) {
	sess, err := convert.SessionProtoToCupsoup(req.GetSession())
	if err != nil {
		return nil, err
	}

	cl := cupsoup.NewClient(cupsoup.WithSession(sess))

	appointments, err := cl.PendingAppointments(ctx)
	if err != nil {
		return nil, err
	}

	protoAppointments := make([]*pb.Appointment, len(appointments))

	for i := range appointments {
		var appointmentProto *pb.Appointment
		appointmentProto, err = convert.AppointmentCupsoupToProto(&appointments[i])
		if err != nil {
			return nil, err
		}

		protoAppointments[i] = appointmentProto
	}

	protoSess, err := convert.SessionCupsoupToProto(cl.Session())
	if err != nil {
		return nil, err
	}

	return &pb.PendingAppointmentsResponse{
		Session:      protoSess,
		Appointments: protoAppointments,
	}, nil
}
