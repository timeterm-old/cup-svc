package grpctx

import (
	"context"
	"errors"
	"strings"

	"github.com/rs/zerolog/log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	"gitlab.com/timeterm/cup-svc/internal/database/daos"
	"gitlab.com/timeterm/cup-svc/internal/transport/grpc/convert"
	"gitlab.com/timeterm/cup-svc/pkg/cupsoup"
	pb "gitlab.com/timeterm/proto/go/cup"
)

// SignIn signs an existing user in. See the equivalent in cupsoup: cupsoup.Client.SignIn.
func (c CUPServer) SignIn(req *pb.SignInRequest, srv pb.CUP_SignInServer) error {
	cl := cupsoup.NewClient()

	err := cl.Init(srv.Context())
	if err != nil {
		log.Error().Err(err).Msg("could not initialize the cupsoup client")
		return status.Error(codes.Internal, "could not initialize the CUP client")
	}

	ud, ok := daos.GetUserDAO(c.base.Reg)
	if !ok {
		log.Error().Msg("could not get user dao")
		return status.Error(codes.Internal, "could not find data access object")
	}

	usess, err := c.validateSession(srv.Context(), req.GetTtSessionId())
	if err != nil {
		return err
	}

	cupName, err := ud.GetCUPName(usess.GetCardId())
	if err != nil {
		return status.Error(codes.NotFound, "user not found")
	}

	err = srv.Send(&pb.SignInEvent{Type: pb.SE_SEARCHING_NAME})
	if err != nil {
		return err
	}

	if len(cupName) < 3 {
		return status.Error(codes.InvalidArgument, "invalid CUP name")
	}
	options, err := cl.SearchUsers(srv.Context(), strings.ToLower(cupName[:3]))
	if err != nil {
		return status.Error(codes.Internal, "failed searching userss")
	}

	err = srv.Send(&pb.SignInEvent{Type: pb.SE_SELECTING_USER})
	if err != nil {
		return err
	}

	candidate, err := selectUser(cupName, options)
	if err != nil {
		return err
	}

	err = srv.Send(&pb.SignInEvent{Type: pb.SE_SIGNING_IN, SelectedOpt: candidate})
	if err != nil {
		return err
	}

	err = cl.SignIn(srv.Context(), candidate, req.GetPin(), "")
	if err != nil {
		sess, convertErr := convert.SessionCupsoupToProto(cl.Session())
		if convertErr != nil {
			return convertErr
		}

		sendErr := srv.Send(&pb.SignInEvent{
			Type: pb.SE_FAILED,

			// Useful for retrying.
			Session: sess,
		})
		if sendErr != nil {
			return sendErr
		}

		return processSignInError(srv.Context(), err)
	}

	sess, err := convert.SessionCupsoupToProto(cl.Session())
	if err != nil {
		return err
	}

	err = srv.Send(&pb.SignInEvent{
		Type:    pb.SE_SIGNED_IN,
		Session: sess,
	})
	return err
}

// SignIn signs a user in. A valid session used with SignIn before must be provided.
// See the equivalent in cupsoup: cupsoup.Client.SignInRetry.
func (c CUPServer) SignInRetry(req *pb.SignInRetryRequest, srv pb.CUP_SignInRetryServer) error {
	sess, err := convert.SessionProtoToCupsoup(req.GetSession())
	if err != nil {
		return err
	}

	cl := cupsoup.NewClient(cupsoup.WithSession(sess))

	err = srv.Send(&pb.SignInEvent{Type: pb.SE_SEARCHING_NAME})
	if err != nil {
		return err
	}

	err = srv.Send(&pb.SignInEvent{Type: pb.SE_SIGNING_IN})
	if err != nil {
		return err
	}

	err = cl.SignIn(srv.Context(), req.GetSelectedOpt(), req.GetPin(), req.GetCaptchaText())
	if err != nil {
		var sess *pb.Session
		sess, err = convert.SessionCupsoupToProto(cl.Session())
		if err != nil {
			return err
		}

		err = srv.Send(&pb.SignInEvent{
			Type: pb.SE_FAILED,

			// Useful for retrying.
			Session: sess,
		})
		if err != nil {
			return err
		}

		return processSignInError(srv.Context(), err)
	}

	protoSess, err := convert.SessionCupsoupToProto(cl.Session())
	if err != nil {
		return err
	}

	err = srv.Send(&pb.SignInEvent{
		Type:    pb.SE_SIGNED_IN,
		Session: protoSess,
	})
	return err
}

func processSignInError(ctx context.Context, err error) error {
	if captchaErr, ok := err.(cupsoup.CaptchaError); ok {
		err = grpc.SetTrailer(ctx, metadata.New(map[string]string{
			"cup-captcha-url": captchaErr.URL,
		}))
		if err != nil {
			return status.Error(codes.Internal, "could not send captcha guid trailer")
		}
		return status.Error(codes.PermissionDenied, "could not sign in: invalid captcha")
	}

	var ce cupsoup.CUPError
	if errors.As(err, &ce) {
		return status.Error(codes.PermissionDenied, "could not sign in: "+string(ce))
	}

	return status.Error(codes.PermissionDenied, "could not sign in")
}

// TODO: what if two people have the same name?
func selectUser(cupName string, options map[string]string) (string, error) {
	for index, opt := range options {
		if strings.Contains(opt, cupName) {
			return index, nil
		}
	}
	return "", status.Error(codes.NotFound, "could not find CUP user")
}
